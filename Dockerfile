### BUILD IMAGE
FROM node:13.12.0-alpine AS builder
LABEL MAINTAINER="developers@statful.com"

ARG GIT_SSH_KEY
# Put ssh key in .ssh directory
RUN mkdir /root/.ssh/ && \
    echo "${GIT_SSH_KEY}" >> /root/.ssh/id_rsa && \
    # Pre authorize git repository
    ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts && \
    echo 'Host bitbucket.org\nHostName bitbucket.org\nIdentityFile /root/.ssh/id_rsa\nUser git' >> /root/.ssh/config && \
    # Set key permissions
    chmod go-w /root && \
    chmod 700 /root/.ssh && \
    chmod 400 /root/.ssh/id_rsa && \
    chmod 400 /root/.ssh/known_hosts

WORKDIR /statful-demo

# Copy package.json and download dependencies
ADD package.json package.json
ADD yarn.lock yarn.lock

RUN yarn install

ADD public/ public/
ADD src/ src/

RUN yarn build 


### FINAL image
FROM nginx:1.17-alpine

WORKDIR /opt/statful-demo/

COPY --from=builder /statful-demo/ .

EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]