import React from 'react';
import logo from './logo.png';
import StatfulCharts from 'statful-charts';
import './App.css';

export default class App extends React.Component{
   componentDidMount(){
    StatfulCharts.setStyles({
        "fontFamily": "Lato",
        "titleColor": "#354052",
        "gridYColor": "#E6EAEE",
        "axisValueColor": "rgba(74, 74, 94, 0.5)",
        "axisValueFontSize": "12px",
        "legendFontSize": "14px",
        "legendLabelColor": "#4A4A5E",
        "tooltipTextColor": "#4A4A5E"
      });
    this.generateTotatlRevenue();
    this.generateRevenueBySport();
    this.generateBetsBySport();
    this.generateAverageAmountBySport();
    this.generateTotalBetBySport();
   } 

   componentWillUnmount(){

   }
   
   generateGraphs (numberOfItems, graphType) {
    var result = [];
    for (var i = 0; i < numberOfItems; i++) {
        result.push({
            graphType: graphType
        });
    }
    return result;
  }

  render(){
    return (
      <div className="App">
          <div className="left-menu">
              <div className="logo">
                <img src={logo}  alt="logo" />
              </div>
              <ul>
                  <li className="menu">
                      <a className="available" href="#">
                          <i className="fa fa-home" aria-hidden="true"></i>
                          DASHBOARD
                      </a>
                  </li>
                  <li className="menu">
                      <a className="available" href="#">
                          <i className="fa fa-user" aria-hidden="true"></i>
                          ICE Demo
                      </a>
                  </li>
              </ul>
          </div>
          <div className="right-menu">
              <div className="account">
                  <div className="title">
                      Revenue summary
                  </div>

                  <div className="name">
                      Demo User
                  </div>
              </div>

              <div className="view-container">

                  <div className="widget-container">
                      <div id="bigNumber"></div>
                  </div>

                  <div className="widget-container">
                      <div id="donut"></div>
                  </div>

                  <div className="widget-container">
                      <div id="stackedColumn"></div>
                  </div>
      
                  <div className="widget-container">
                      <div id="smoothedLine"></div>
                  </div>

                  <div className="widget-container">
                      <div id="stackedLine"></div>
                  </div>

              </div>
          </div>
      </div>
    );
  }

 generateTotatlRevenue(){
  var result = [];
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  const data = [{"metric":"application.timer.bet.value.sum","metricId":"yrmnpo0omvlhtkd3jxzcf7","tags":[],"aggregation":{"name":"sum","frequencies":[10]},"resolution":"1h","from":"2020-04-26T09:33:02+00:00","to":"2020-04-28T09:33:02+00:00"}];
  fetch( proxyurl + 'https://api-beta.statful.com/api/query/v1/metrics/values?multiple=true', 
      {  
          method: 'POST', 
          headers: {
          'Content-Type': 'application/json',
          'M-Api-Token' : '70e9f957-117a-427c-a6cd-9c29d4b5b856	',
          },
          body:JSON.stringify(data)
      })
      .then((response) => {
          return response.json();
      })
      .then((data) => {
          for (var i = 0; i < data[0].values.length; i++) {
              var serie = {};
              serie.values = data[0].values[i];
              serie.name =  data[0].timestamps[i];
              result.push(serie);
          }
          StatfulCharts.render([
              {
                  id: "bigNumber",
                  element: "#bigNumber",
                  graphType: "bigNumber",
                  title: "Total Revenue",
                  data: result,
                  graphType: 'BIGNUMBER',
                  currency: 'EUR',
              },
          ]);    
      });  
}

 generateRevenueBySport(){
  var result = [];
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  const data = [{"metric":"application.timer.bet.value.sum","metricId":"8u2bq9xv4elwpexylnwk6n","tags":[],"aggregation":{"name":"sum","frequencies":[10],"transformation":"sum"},"resolution":"1h","groupBy":["sport"],"from":"2020-04-26T10:15:22+00:00","to":"2020-04-28T10:15:22+00:00","alias":{"application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportAmericanFootball":"AmericanFootball","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportBaseball":"Baseball","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportBasketball":"Basketball","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportCricket":"Cricket","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportFootball":"Football","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportHorseRacing":"HorseRacing","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportRugby":"Rugby"}}];
  fetch( proxyurl + 'https://api-beta.statful.com/api/query/v1/metrics/values?multiple=true', 
      {  
          method: 'POST', 
          headers: {
          'Content-Type': 'application/json',
          'M-Api-Token' : '70e9f957-117a-427c-a6cd-9c29d4b5b856	',
          },
          body:JSON.stringify(data)
      })
      .then((response) => {
          return response.json();
      })
      .then((data) => {
          var graphTypes  = data.length;
          for (var i = 0; i < data.length; i++) {
              var serie = {};
              serie.name =  data[i].groupBy.sport;
              var sum = 0;
              for(var j =0; j < data[i].values.length; j++){
                  sum+= data[i].values[j];
              }
              serie.values = sum;  
              result.push(serie);
          }
          StatfulCharts.render([
              {
                  id: "donut",
                  element: "#donut",
                  title: "Overview Revenue by sport",
                  data: result,
                  graphs: this.generateGraphs(graphTypes, 'DONUT'),
                  legend: {
                      enabled: true,
                      align: 'right'
                  },
                  tooltip: {
                      enabled: true,
                      dataType: "PERCENT"+"VALUE",
                      decimalPlaces: 0
                  }
              },
          ]);    
      });  
}


 generateBetsBySport(){
  var result = [];
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  const data = [{"metric":"application.timer.bet.value.sum","metricId":"8u2bq9xv4elwpexylnwk6n","tags":[],"aggregation":{"name":"sum","frequencies":[10],"transformation":"sum"},"resolution":"1h","groupBy":["sport"],"from":"2020-04-26T10:15:22+00:00","to":"2020-04-28T10:15:22+00:00","alias":{"application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportAmericanFootball":"AmericanFootball","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportBaseball":"Baseball","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportBasketball":"Basketball","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportCricket":"Cricket","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportFootball":"Football","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportHorseRacing":"HorseRacing","application.timer.bet.value.sum8u2bq9xv4elwpexylnwk6nsumsumsportRugby":"Rugby"}}];
  fetch( proxyurl + 'https://api-beta.statful.com/api/query/v1/metrics/values?multiple=true', 
      {  
          method: 'POST', 
          headers: {
          'Content-Type': 'application/json',
          'M-Api-Token' : '70e9f957-117a-427c-a6cd-9c29d4b5b856	',
          },
          body:JSON.stringify(data)
      })
      .then((response) => {
          return response.json();
      })
      .then((data) => {
          var graphTypes  = data.length;
          for (var i = 0; i < data.length; i++) {
              var serie = {};
              serie.timestamps = data[i].timestamps;
              serie.values = data[i].values;
              serie.name =  data[i].groupBy.sport;
              result.push(serie);
          }
          StatfulCharts.render([
              {
                  id: "stackedColumn",
                  element: "#stackedColumn",
                  title: "Number of bets by sport",
                  data: result,
                  graphs: this.generateGraphs(graphTypes, 'COLUMN_STACKED'),
                  axis: {
                      showYAxis: false,
                      showXAxis: false,
                      verticalAxisMin: 0
                  },
                  grid: {
                      showYGrid: true
                  },
                  legend: {
                      enabled: true,
                      align: 'right'
                  },
                  tooltip: {
                      enabled: true,
                      decimalPlaces: 0
                  }
              }
          ]);    
      });  
}


 generateAverageAmountBySport(){
  var result = [];
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  const data = [{"metric":"application.timer.bet.value.sum","metricId":"azh9up7ux8rouzhgtu387r","tags":[],"aggregation":{"name":"sum","frequencies":[10],"transformation":"mean"},"resolution":"1h","groupBy":["sport"],"from":"2020-04-26T11:23:52+00:00","to":"2020-04-28T11:23:52+00:00","alias":{"application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportbasketball":"basketball","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportboxing":"boxing","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportcricket":"cricket","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportfootbal":"football","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansporthockey":"hockey","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportsnooker":"snooker","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportgolf":"golf","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansporttennis":"tennis","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportFootbal":"American Footbal","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportSumo":"Sumo","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportBaseball":"Baseball","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportAmericanFootball":"American Football","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportBasketball":"Basketball","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportFootball":"Football","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportCricket":"Cricket","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportHorseRacing":"Horse Racing","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportRugby":"Rugby","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportSoccer":"Soccer","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportTennis":"Tennis"}}];
  fetch( proxyurl + 'https://api-beta.statful.com/api/query/v1/metrics/values?multiple=true', 
      {  
          method: 'POST', 
          headers: {
          'Content-Type': 'application/json',
          'M-Api-Token' : '70e9f957-117a-427c-a6cd-9c29d4b5b856	',
          },
          body:JSON.stringify(data)
      })
      .then((response) => {
          return response.json();
      })
      .then((data) => {
          var graphTypes  = data.length;
          for (var i = 0; i < data.length; i++) {
              var serie = {};
              serie.timestamps = data[i].timestamps;
              serie.values = data[i].values;
              serie.name =  data[i].groupBy.sport;
              result.push(serie);
          }
          StatfulCharts.render([
              {
                  id: "smoothedLine",
                  element: "#smoothedLine",
                  title: "Average bet amount by sport",
                  data: result,
                  graphs: this.generateGraphs(graphTypes, 'LINE_SMOOTHED'),
                  axis: {
                      showYAxis: false,
                      showXAxis: false,
                      verticalAxisMin: 0
                  },
                  grid: {
                      showYGrid: true
                  },
                  legend: {
                      enabled: true,
                      align: 'right'
                  },
                  drawPoints: 'CONNECTED',
                  tooltip: {
                      enabled: true,
                      decimalPlaces: 0
                  }
              }
          ]);    
      });  
}


 generateTotalBetBySport(){
  var result = [];
  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  const data = [{"metric":"application.timer.bet.value.sum","metricId":"azh9up7ux8rouzhgtu387r","tags":[],"aggregation":{"name":"sum","frequencies":[10],"transformation":"mean"},"resolution":"1h","groupBy":["sport"],"from":"2020-04-26T11:23:52+00:00","to":"2020-04-28T11:23:52+00:00","alias":{"application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportbasketball":"basketball","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportboxing":"boxing","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportcricket":"cricket","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportfootbal":"football","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansporthockey":"hockey","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportsnooker":"snooker","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportgolf":"golf","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansporttennis":"tennis","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportFootbal":"American Footbal","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportSumo":"Sumo","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportBaseball":"Baseball","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportAmericanFootball":"American Football","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportBasketball":"Basketball","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportFootball":"Football","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportCricket":"Cricket","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportHorseRacing":"Horse Racing","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportRugby":"Rugby","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportSoccer":"Soccer","application.timer.bet.value.sumazh9up7ux8rouzhgtu387rsummeansportTennis":"Tennis"}}];
  fetch( proxyurl + 'https://api-beta.statful.com/api/query/v1/metrics/values?multiple=true', 
      {  
          method: 'POST', 
          headers: {
          'Content-Type': 'application/json',
          'M-Api-Token' : '70e9f957-117a-427c-a6cd-9c29d4b5b856	',
          },
          body:JSON.stringify(data)
      })
      .then((response) => {
          return response.json();
      })
      .then((data) => {
          var graphTypes  = data.length;
          for (var i = 0; i < data.length; i++) {
              var serie = {};
              serie.timestamps = data[i].timestamps;
              serie.values = data[i].values;
              serie.name =  data[i].groupBy.sport;
              result.push(serie);
          }
          StatfulCharts.render([
              {
                  id: "stackedLine",
                  element: "#stackedLine",
                  title: "Total bet amount by sport",
                  data: result,
                  graphs: this.generateGraphs(graphTypes, 'LINE_STACKED'),
                  axis: {
                      showYAxis: false,
                      showXAxis: false,
                      verticalAxisMin: 0
                  },
                  grid: {
                      showYGrid: true
                  },
                  legend: {
                      enabled: true,
                      align: 'right'
                  },
                  drawPoints: 'CONNECTED',
                  tooltip: {
                      enabled: true,
                      decimalPlaces: 0
                  }
              }
          ]);    
      });  
}

}

