pipeline {
    agent {
        kubernetes {
            label 'statful-charts-build'
            yamlFile 'jenkins-agent.yaml'
        }
    }

    environment {
        GIT_COMMIT = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
        GIT_REPO = sh(returnStdout: true, script: 'basename `git config --get remote.origin.url` .git').trim()

        DOCKER_REPO_BASENAME = "083132768368.dkr.ecr.us-west-2.amazonaws.com/statful"
        DOCKER_REPO_NAME = "${GIT_REPO}"
        DOCKER_IMAGE_VERSION = "v${GIT_COMMIT}"
    }

    stages {
        stage('Build') {
            environment {
                SSH_KEY = credentials("jenkins-ssh")
            }
            steps {
                container('ssh-client') {
                    sh '''
                        echo "SSH private key is located at ${SSH_KEY}"
                        echo "SSH user is ${SSH_KEY_USR}"
                        echo "SSH passphrase is ${SSH_KEY_PSW}"
                        ssh-keygen -p -P "${SSH_KEY_PSW}" -N "" -f "${SSH_KEY}"
                    '''
                }
                container('docker') {
                    sh '''
                        docker build -t ${DOCKER_REPO_BASENAME}/${DOCKER_REPO_NAME}:${DOCKER_IMAGE_VERSION} --build-arg GIT_SSH_KEY="$(cat ${SSH_KEY})" .
                    '''
                }
            }
        }
        stage('Push') {
            environment {
                AWS_ID = credentials("jenkins-aws-access-key")
                AWS_ACCESS_KEY_ID = "${env.AWS_ID_USR}"
                AWS_SECRET_ACCESS_KEY = "${env.AWS_ID_PSW}"
            }
            steps {
                container('awscli') {
                    sh "aws ecr get-login --no-include-email --region us-west-2 > /tmp/docker-login.sh"
                }
                container('docker') {
                    sh "source /tmp/docker-login.sh"
                    sh "docker push ${DOCKER_REPO_BASENAME}/${DOCKER_REPO_NAME}:${DOCKER_IMAGE_VERSION}"
                }
            }
        }
    }
}